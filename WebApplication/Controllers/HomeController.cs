﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class HomeController : Controller
    {
        private const string userFilePath = @"D:\UserFile.txt";
        private const string historyFilePath = @"D:\HistoryFile.txt";

        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        public JsonResult GetUser()
        {
            var userModel = getUserModel();
            return Json(userModel, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTable()
        {
            var actions = new List<ActionModel>();

            if (!System.IO.File.Exists(historyFilePath))
            {
                using (FileStream fs = System.IO.File.Create(historyFilePath))
                {
                    Byte[] info = new UTF8Encoding(true).GetBytes("");
                    fs.Write(info, 0, info.Length);
                }
            }
            else
            {
                using (StreamReader sr = System.IO.File.OpenText(historyFilePath))
                {
                    string line = "";
                    while ((line = sr.ReadLine()) != null)
                    {
                        actions.Add(Newtonsoft.Json.JsonConvert.DeserializeObject<ActionModel>(line));
                    }
                }
            }

            return Json(actions, JsonRequestBehavior.AllowGet);
        }

        public bool PostAction(string action, string value)
        {
            var _value = Convert.ToInt32(value);
            var user = getUserModel();

            if (action == "credit") {
                user.Amount -= _value;
                if (user.Amount < 0)
                {
                    return false;
                }
            }
            else
            {
                user.Amount +=_value;
            }

            try
            {
                var newAction = new ActionModel { Date = DateTime.Now.ToShortDateString().ToString(), Action = action, Value = _value };
                using (StreamWriter sw = System.IO.File.AppendText(historyFilePath))
                {
                    var json = Newtonsoft.Json.JsonConvert.SerializeObject(newAction);
                    sw.WriteLine(json);
                }

                using (StreamWriter sw = System.IO.File.CreateText(userFilePath))
                {
                    var json = Newtonsoft.Json.JsonConvert.SerializeObject(user);
                    sw.WriteLine(json);
                }

            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        private UserModel getUserModel()
        {
            var userModel = new UserModel { FirstName = "first name", LastName = "last name", Amount = 0 };

            if (!System.IO.File.Exists(userFilePath))
            {
                using (FileStream fs = System.IO.File.Create(userFilePath))
                {
                    var json = Newtonsoft.Json.JsonConvert.SerializeObject(userModel);
                    Byte[] info = new UTF8Encoding(true).GetBytes(json);
                    fs.Write(info, 0, info.Length);
                }
            }
            else
            {
                using (StreamReader sr = System.IO.File.OpenText(userFilePath))
                {
                    var line = sr.ReadLine();
                    userModel = Newtonsoft.Json.JsonConvert.DeserializeObject<UserModel>(line);
                }
            }

            return userModel;
        }
    }
}
