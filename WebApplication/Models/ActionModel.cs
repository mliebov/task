﻿namespace WebApplication.Models
{
    public class ActionModel
    {
        public string Date { get; set; }
        public string Action { get; set; }
        public int Value { get; set; }
    }
}