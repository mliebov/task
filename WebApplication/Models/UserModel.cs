﻿namespace WebApplication.Models
{
    public class UserModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Amount { get; set; }
    }
}